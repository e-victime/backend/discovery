cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .m2/repository

variables:
  REPO_NAME: gitlab.com/$CI_PROJECT_PATH
  GSG_RELEASE_BRANCHES: master
  DOCKER_DRIVER: overlay
  SPRING_PROFILES_ACTIVE: ci
  MAVEN_OPTS: " -Dmaven.repo.local=./.m2/repository"
  RELEASE_FILE: "target/discovery.jar"

stages:
  - version
  - build
  - test
  - package
  - deploy

before_script:
  - echo "Start CI/CD"

version:
  stage: version
  image: alpine:latest
  before_script:
    - apk add wget
    - wget https://juhani.gitlab.io/go-semrel-gitlab/download/v0.21.1/release
    - chmod +x release
  script:
    - ./release next-version --allow-current > .next-version
  artifacts:
    paths:
      - .next-version
  cache:
    paths:
      - ./release
  only:
    - master

maven-build:
  image: maven:3-jdk-8
  stage: build
  before_script:
    - wget https://juhani.gitlab.io/go-semrel-gitlab/download/v0.21.1/release
    - chmod +x release
    - 'which ssh-agent || ( apt-get update -qy && apt-get install openssh-client -qqy )'
    - eval `ssh-agent -s`
    - echo "${DEPLOYMENT_KEY}" | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - git config --global user.email "contact@antoinethys.com"
    - git config --global user.name "Gitlab CI"
    - sed -i "13s/.*/       <version>$(<.next-version)<\/version>/" pom.xml
    - git add pom.xml
    - git commit -m "Change version in POM from $CI_COMMIT_SHORT_SHA [skip ci]" || echo "No changes, nothing to commit!"
    - git remote rm origin && git remote add origin git@gitlab.com:$CI_PROJECT_PATH.git
    - git push origin HEAD:$CI_COMMIT_REF_NAME
  script:
    - mvn package -B
    - ./release test-git --list-other-changes || true
    - ./release test-api
    - ./release -v
    - ./release help
    - echo "RELEASE_URL=https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs/$CI_JOB_ID/artifacts/discovery.jar" > build_info
    - echo "RELEASE_DESC=\"Micro-Service Discovery\"" >> build_info
    - echo "RELEASE_SHA=$CI_COMMIT_SHA" >> build_info
    - echo "RELEASE_VERSION=$(<.next-version)" >> build_info
  cache:
    paths:
      - ./release
  artifacts:
    paths:
      - target/discovery.jar
      - build_info

test:
  stage: test
  image: maven:3-jdk-8
  script:
    - mvn test

package:
  stage: package
  image: docker:stable
  variables:
    DOCKER_DRIVER: overlay2
    IMAGE_TAG: $CI_REGISTRY_IMAGE
  services:
    - docker:dind
  script:
    - rm -f release_info
    - mv build_info release_info
    - . release_info

    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker tag $IMAGE_TAG $IMAGE_TAG:${CI_COMMIT_SHORT_SHA}
    - docker tag $IMAGE_TAG $IMAGE_TAG:latest
    - docker push $IMAGE_TAG

    - ./release changelog
    - ./release commit-and-tag CHANGELOG.md release_info
    - ./release --ci-commit-tag v$RELEASE_VERSION add-download -f "$RELEASE_FILE" -d "$RELEASE_DESC"
  cache:
    paths:
      - ./release
  only:
    - master

deploy:
  stage: package
  image: maven:3.3.9-jdk-8
  script:
    - 'mvn deploy -s ci_settings.xml'
  only:
    - master

k8s-deploy:
  image: google/cloud-sdk
  stage: deploy
  script:
    - echo "$KUBE_CONFIG" > kubeconfig
    - kubectl --kubeconfig kubeconfig apply -f deployment.yml --namespace=e-victime
    - kubectl -n e-victime rollout restart deploy discovery-backend
  only:
    - master

after_script:
  - echo "End CI/CD"
