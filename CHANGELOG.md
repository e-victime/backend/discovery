# CHANGELOG

<!--- next entry here -->

## 1.1.9
2020-05-26

### Fixes

- **discovery:** Change zone URL (50da8ea6ad0a8846258ff90a8de160c85e159ac6)

## 1.1.8
2020-05-26

### Fixes

- **discovery:** Disable connexion to himself (334b6680d11188c9d2995267c355cc8797d22041)

## 1.1.7
2020-05-25

### Fixes

- **config:** Change URL (60dd70774d4e2a30013d30003dd5120b6166c6c1)

## 1.1.6
2020-05-25

### Fixes

- **ingress:** Add Discovery ingress (07749dbcca65b79713ab3288a247302b48144a21)

## 1.1.5
2020-05-25

### Fixes

- **config:** Change config files usages (c632f188adb09af7551d8e9730001906c2d797fe)

## 1.1.4
2020-05-25

### Fixes

- **discovery:** Change Hostname (b40f7355cf9f8d10b4690d1e9346ad2419481ecb)

## 1.1.3
2020-05-25

### Fixes

- **discovery:** Repair Disocvery Config (9a8f4d4c9e972cff178f6a4eeedcbe60fb6f868f)

## 1.1.2
2020-05-20

### Fixes

- **ci:** Launch new clean ci (18467c043ed146da3c3a742e86c78fd96fd1882a)

## 1.1.1
2020-05-20

### Fixes

- **external:** fix external api access (d87b161cbfa215a8a21ecb8f72d6d6775f3bc1fa)

## 1.1.0
2020-05-11

### Features

- **deploy:** Go on kubernetes (0368ac417f7b47fad477dbfe3e28b49b3f1b8fe5)
- **ci:** Add tests on maven package (7aa934d750a13d21491c0a7cd13504c9b051080f)
- **ci:** Fix all CI (dda0f7ce1f4bbcbc346ad1d7b4dbbac0f684fd2f)

### Fixes

- **deployment:** Change registry url (2fca8b1abb9166b3aa60a777be2ff2a9779868c6)
- **monitoring:** Expose all actuator routes (fa4add824c9149284c18bfbbd690c6294cf2bc19)
- **ci:** Repair changelog for ci (20adde5f22d41cf9d9eafa374a919a7cc9df4bff)
- **ci:** repair ci (67054a4c0ece260f4f46ed250beba63d916c594a)
- **ci:** Remove command in ci (3ad2db3fa4b207a5c6939a173851eca982d22b05)
- **ci:** Repair ci (17d96e66aacc92c3faa1f847661f8d3c8873693d)