FROM openjdk:8-jdk-alpine

VOLUME /tmp
ADD /target/discovery.jar app.jar

EXPOSE 8383

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]